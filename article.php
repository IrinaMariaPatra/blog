<!DOCTYPE html>
<html lang="en">
<?php include "config.php" ?>
<?php include "parts/head.php" ?>
<?php include "functions.php"?>
<body>
<div class="container" style="background-color: floralwhite">
    <?php include "parts/header.php" ?>
    <div class="row">
        <div id="content" class="col-9">
            <?php
                    //$result = mysqli_query($dbConnection, "SELECT * FROM stire WHERE id=".$_GET['id']);
                     $s=dbSelect('stire',['id'=>$_GET['id']],[]);
                    // $s = $result->fetch_assoc();
            ?>
            <h1><?php echo $s[0]['title']; ?></h1>
            <hr />
            <div class="article-image">
                <img class="img-fluid" src="<?php echo $s[0]['image']; ?>" />
            </div>
            <div class="article-content">
                <?php echo $s[0]['continut']; ?>
            </div>
            <hr />
            <?php
           // $result = mysqli_query($dbConnection, "SELECT * FROM comments WHERE article_id=".$_GET['id']);
           // $comments = $result->fetch_all(MYSQLI_ASSOC);
            $comments=dbSelect('comments',['article_id'=>$_GET['id']],[]);
            ?>
            <div class="article-comments">
                <?php foreach ($comments as $comment):?>
                    <div class="alert alert-success" role="alert">
                        <a href="#" class="badge badge-warning"><?php echo $comment['nickname'] ?></a>
                        <?php echo $comment['content'] ?>
                    </div>
                <?php endforeach;?>
            </div>
            <hr />
            <div class="article-form">
                <p>Va rugam sa folositi un limbaj civilizat!!</p>
                <form action="add-coment.php?article_id=<?php echo $_GET['id']; ?>" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nickname</span>
                        </div>
                        <input name="nickname" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Comentariul tau</span>
                        </div>
                        <textarea name="comment" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Adauga comentariu</button>
                </form>
            </div>

        </div>
        <?php include "parts/sidebar.php" ?>
    </div>
    <?php include "parts/footer.php"; ?>
</div>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>