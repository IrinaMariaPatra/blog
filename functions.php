<?php
include "config.php";
function menu($alege){    //afisare header
    global $dbConnection;
    $result = mysqli_query($dbConnection,"SELECT * FROM meniu WHERE alege='$alege'");
    $menuItems = $result->fetch_all(MYSQLI_ASSOC);

    ?>
    <table width="95%" align="center">
        <tr align="center">
            <td>

                    <?php foreach ($menuItems as $menuItem): ?>
                        <th><a href= "<?php echo $menuItem['url']; ?>"> <?php echo $menuItem['title']; ?></a></th>
                    <?php endforeach; ?>

            </td>
        </tr>
    </table>
    <?php
}

function afisareStire($stire){

    //var_dump($stire);

    ?>


                        <u><h2><?php echo $stire['title'];?></h2></u>
                        <?php echo $stire ['continut']; ?>
                            <a href="article.php?id=<?php echo $stire['id'];?>">Citeste mai departe...</a>
                        <br><br><img src="<?php echo $stire['image'] ;?>" width="60%"  </p>


<?php
}
function dbDelete($table, $id){
    global $dbConnection;
    $result = mysqli_query($dbConnection, "DELETE FROM $table WHERE id=".intval($id));

    return mysqli_affected_rows($dbConnection)>0;
}


function dbUpdate($table, $id, $data){
    global $dbConnection;
    $sets = [];
    foreach ($data as $column => $value){
        $sets[]="`".mysqli_real_escape_string($dbConnection,$column)."`='".mysqli_real_escape_string($dbConnection,$value)."'";
    }
    $sqlSets = implode(',', $sets);

    $result = mysqli_query($dbConnection, "UPDATE $table SET $sqlSets  WHERE id=".intval($id));

    return mysqli_affected_rows($dbConnection)>0;

}

function dbInsert($table, $data){


    global $dbConnection;
    $columns = [];
    $values=[];
    foreach ($data as $column => $value){
        $columns[]='`'.mysqli_real_escape_string($dbConnection,$column).'`';
        $values[]='\''.mysqli_real_escape_string($dbConnection,$value).'\'';
    }
    $sqlColumns=implode(',',$columns);
    $sqlValues=implode(',',$values);
    //die( "INSERT INTO $table ($sqlColumns) VALUES ($sqlValues) "); - query care ruleaza
    $result = mysqli_query($dbConnection, "INSERT INTO $table ($sqlColumns) VALUES ($sqlValues) ");
    return mysqli_insert_id($dbConnection);

}

/**
 *  select all -
 *  select by -  WHERE user_id=5 AND active=1
 *  select like -
 *  limit
 *  offset
 *  sorting
 */
function dbSelect($table, $filters=null, $likeFilters=null, $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    global $dbConnection;
    $sql = "SELECT * FROM $table";

    if ($filters!=null || $likeFilters!=null){
        $sets = [];
        foreach ($filters as $column => $value){
            $sets[]="`".mysqli_real_escape_string($dbConnection,$column)."`='".mysqli_real_escape_string($dbConnection,$value)."'";
        }
        foreach ($likeFilters as $column => $value){
            $sets[]="`".mysqli_real_escape_string($dbConnection,$column)."` LIKE '%".mysqli_real_escape_string($dbConnection,$value)."%'";
        }
        $sql.= ' WHERE '.implode(' AND ', $sets);
    }

    if ($sortBy != null) {
        $sql.= ' ORDER BY '.mysqli_real_escape_string($dbConnection,$sortBy).' '.mysqli_real_escape_string($dbConnection,$sortDirection);
    }

    if ($limit!=null){
        $sql.= ' LIMIT '.intval($offset).','.intval($limit);
    }

    $result = mysqli_query($dbConnection, $sql);

    if (!$result){
        die("SQL error: " . mysqli_error($dbConnection)." SQL:".$sql);
    }

    return $result->fetch_all(MYSQLI_ASSOC);
}

function dbSelectOne($table, $filters=null, $likeFilters=null, $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    $data = dbSelect($table, $filters, $likeFilters, 0, 1,  $sortBy, $sortDirection);
    return $data[0];
}