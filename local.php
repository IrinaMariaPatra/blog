<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <meta charset="UTF-8">
    <title>Redactia de stiri</title>
</head>
<body bgcolor="#ffe4c4">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<html lang="en">
<?php include "config.php" ?>
<?php include "parts/head.php" ?>
<?php include "functions.php"?>
<body>
<div class="container" style="background-color: blue">
    <?php include "parts/header.php" ?>
    <div class="row">
        <div id="content" class="col-sm-12 col-lg-9">

            <?php
            //$result = mysqli_query($mysqlConnect,"SELECT * FROM stire WHERE categorie='l'");
            //$stire = $result->fetch_all(MYSQLI_ASSOC);
            $stire=dbSelect('stire',['categorie'=>'l'],[]);
            foreach ($stire as $key => $linie) {
                afisareStire($linie,$key);
            }
            ?>
        </div>
        <?php include "parts/sidebar.php" ?>
    </div>
    <div>
        <?php include "parts/footer.php"; ?>
    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html
