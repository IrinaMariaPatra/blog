<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<div class="row">
    <div class="col-12">
        <div id="logo">
            <img src="images/logo.jpg" width="130">
        </div>
        <div id="menu">

            <ul class="nav nav-tabs">
                <marquee>REDACTIA DE STIRI - Cel mai exact site de stiri din orasul tau!</marquee>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#"><h2>Meniu</h2></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <?php
                            //global $dbConnection;
                            //$result = mysqli_query($dbConnection,"SELECT * FROM meniu WHERE alege='h'");
                            //$menuItems = $result->fetch_all(MYSQLI_ASSOC);
                            //fetch_assoc()-daca vrem decat un singur articol
                            $menuItems=dbSelect('meniu',['alege'=>'h'],[]);
                            ?>
                            <?php foreach ($menuItems as $menuItem): ?>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo $menuItem['url'];?>">
                                        <?php
                                        echo $menuItem['title'];
                                        //partea de comentariu acceptat+respins
                                        ?>
                                        <span class="sr-only">(current)</span></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </nav>
            </ul>

        </div>
    </div>
</div>